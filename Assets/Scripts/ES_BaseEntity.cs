﻿using UnityEngine;
using System.Collections;

public class ES_BaseEntity : MonoBehaviour 
{
	#region Fields
	private float m_energyLevel;
	private float m_movementSpeed;
	private float m_rotationSpeed;
	private float m_jumpSpeed;
	private bool m_canAttack;
	private bool m_canCharge;
	private float m_energyReductionSpeed;
	#endregion

	#region Getters and Setters
	public float energyLevel
	{
		get { return m_energyLevel; }
		set { m_energyLevel = value; }
	}

	public float movementSpeed
	{
		get { return m_movementSpeed; }
		set { m_movementSpeed = value; }
	}

	public float rotationSpeed
	{
		get { return m_rotationSpeed; }
		set { m_rotationSpeed = value; }
	}

	public float jumpSpeed
	{
		get { return m_jumpSpeed; }
		set { m_jumpSpeed = value; }
	}

	public bool canAttack
	{
		get { return m_canAttack; }
		set { m_canAttack = value; }
	}

	public bool canCharge
	{
		get { return m_canCharge; }
		set { m_canCharge = value; }
	}

	public float energyReductionSpeed
	{
		get { return m_energyReductionSpeed; }
		set { m_energyReductionSpeed = value; }
	}
	#endregion

	#region Unity Methods
	public virtual void Awake() {}

	public virtual void Start() {}

	public virtual void Update() 
	{
		if(!IsDead())
		{
			m_energyLevel -= m_energyReductionSpeed * Time.deltaTime;
		}
	}

	public virtual void FixedUpdate() {}
	#endregion

	#region Methods
	public bool IsDead()
	{
		return (m_energyLevel < 0.0f);
	}
	#endregion
}
