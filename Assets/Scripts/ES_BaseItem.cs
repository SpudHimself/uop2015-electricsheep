﻿using UnityEngine;
using System.Collections;

public class ES_BaseItem : MonoBehaviour 
{
	#region Fields
	private float m_rotationSpeed;
	private float m_hoverHeight;
	private float m_fallSpeed;
	#endregion

	#region Getters and Setters
	public float rotationSpeed
	{
		get { return m_rotationSpeed; }
		set { m_rotationSpeed = value; }
	}

	public float hoverHeight
	{
		get { return m_hoverHeight; }
		set { m_hoverHeight = value; }
	}

	public float fallSpeed 
	{
		get { return m_fallSpeed; }
		set { m_fallSpeed = value; }
	}
	#endregion

	#region Unity Methods
	public virtual void Awake()
	{
	}

	public virtual void Start()
	{
		//default initialisation
		m_rotationSpeed = 0.0f;
		m_hoverHeight = 0.0f;
		m_fallSpeed = 0.0f;
	}

	public virtual void Update()
	{
		RotateObject();
	}
	#endregion

	#region Methods
	private void RotateObject()
	{
		transform.Rotate (Vector3.up * m_rotationSpeed * Time.deltaTime);
	}

	public bool AboutToCollide()
	{
		Debug.DrawRay (transform.position, Vector3.down.normalized * m_hoverHeight);

		if (Physics.Raycast(transform.position, Vector3.down, m_hoverHeight))
		{
			return true;
		}

		return false;
	}
	#endregion
}
