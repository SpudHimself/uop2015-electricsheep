﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ES_ShootButtonScript : MonoBehaviour 
{
	public GameObject player1;
	public GameObject player2;
	private ES_PlayerEntity m_entity1;
	private ES_PlayerEntity m_entity2;
	
	public Image P1Attack;
	public Image P2Attack;
	public Image P1Charge;
	public Image P2Charge;
	
	public Image P1Health0;
	public Image P1Health1;
	public Image P1Health2;
	public Image P1Health3;
	public Image P1Health4;
	public Image P1Health5;
	public Image P1Health6;
	public Image P1Health7;
	
	public Image P2Health0;
	public Image P2Health1;
	public Image P2Health2;
	public Image P2Health3;
	public Image P2Health4;
	public Image P2Health5;
	public Image P2Health6;
	public Image P2Health7;
	
	public ParticleSystem P1Sparks;
	public ParticleSystem P2Sparks;

	public Text player1WinText;
	public Text player2WinText;

	public Button backButton;
	public Text backButtonText;
	// Use this for initialization
	void Start () 
	{
		P1Attack.enabled = false;
		P2Attack.enabled = false;
		
		P1Charge.enabled = false;
		P2Charge.enabled = false;
		
		P1Health0.enabled = true;
		P1Sparks.enableEmission = false;

		player1WinText.enabled = false;
		player2WinText.enabled = false;
		backButton.enabled = false;
		backButtonText.enabled = false;
	}
	
	void Awake ()
	{
		m_entity1 = player1.gameObject.GetComponent<ES_PlayerEntity> ();
		m_entity2 = player2.gameObject.GetComponent<ES_PlayerEntity> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//#########################################player1 checks
		
		//check if the player can fire
		if (m_entity1.canAttack == true)
		{
			P1Attack.enabled = true;
		}
		else
		{
			P1Attack.enabled = false;
		}
		
		//check if the player can charge
		if (m_entity1.canCharge == true)
		{
			P1Charge.enabled = true;
		}
		else
		{
			P1Charge.enabled = false;
		}
		
		//check the player health
		// 100, 87.5, 75, 62.5, 50, 37.5, 25, 12.5, 0
		if (m_entity1.energyLevel <= 100 && m_entity1.energyLevel > 87.5) 
		{
			P1Health0.enabled = true;
			P1Health1.enabled = false;
			P1Health2.enabled = false;
			P1Health3.enabled = false;
			P1Health4.enabled = false;
			P1Health5.enabled = false;
			P1Health6.enabled = false;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = false;
			P1Sparks.startSize = 1;
		} 
		if (m_entity1.energyLevel < 87.5 && m_entity1.energyLevel > 75) 
		{
			P1Health0.enabled = false;
			P1Health1.enabled = true;
			P1Health2.enabled = false;
			P1Health3.enabled = false;
			P1Health4.enabled = false;
			P1Health5.enabled = false;
			P1Health6.enabled = false;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = false;
			P1Sparks.startSize = 1;
			
		} 
		if (m_entity1.energyLevel < 75 && m_entity1.energyLevel > 62.5) 
		{
			P1Health0.enabled = false;
			P1Health1.enabled = false;
			P1Health2.enabled = true;
			P1Health3.enabled = false;
			P1Health4.enabled = false;
			P1Health5.enabled = false;
			P1Health6.enabled = false;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = false;
			P1Sparks.startSize = 1;
		} 
		if (m_entity1.energyLevel < 62.5 && m_entity1.energyLevel > 50) 
		{
			P1Health0.enabled = false;
			P1Health1.enabled = false;
			P1Health2.enabled = false;
			P1Health3.enabled = true;
			P1Health4.enabled = false;
			P1Health5.enabled = false;
			P1Health6.enabled = false;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = false;
			P1Sparks.startSize = 1;
		}
		if (m_entity1.energyLevel < 50 && m_entity1.energyLevel > 37.5)
		{
			P1Health0.enabled = false;
			P1Health1.enabled = false;
			P1Health2.enabled = false;
			P1Health3.enabled = false;
			P1Health4.enabled = true;
			P1Health5.enabled = false;
			P1Health6.enabled = false;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = true;
			P1Sparks.startSize = 1;
		}
		if (m_entity1.energyLevel < 37.5 && m_entity1.energyLevel > 25) 
		{
			P1Health0.enabled = false;
			P1Health1.enabled = false;
			P1Health2.enabled = false;
			P1Health3.enabled = false;
			P1Health4.enabled = false;
			P1Health5.enabled = true;
			P1Health6.enabled = false;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = true;
			P1Sparks.startSize = 1;
		} 
		if (m_entity1.energyLevel < 25 && m_entity1.energyLevel > 0)
		{
			P1Health0.enabled = false;
			P1Health1.enabled = false;
			P1Health2.enabled = false;
			P1Health3.enabled = false;
			P1Health4.enabled = false;
			P1Health5.enabled = false;
			P1Health6.enabled = true;
			P1Health7.enabled = false;
			P1Sparks.enableEmission = true;
			P1Sparks.startSize = 1;
			
		} 
		if (m_entity1.energyLevel < 12.5 && m_entity1.energyLevel > 0) 
		{
			P1Sparks.startSize = 3;
		}
		if (m_entity1.energyLevel <= 0) //dead state
		{
			P1Health0.enabled = false;
			P1Health1.enabled = false;
			P1Health2.enabled = false;
			P1Health3.enabled = false;
			P1Health4.enabled = false;
			P1Health5.enabled = false;
			P1Health6.enabled = false;
			P1Health7.enabled = true;
			P1Sparks.enableEmission = false;

			player2WinText.enabled = true;
			backButton.enabled = true;
			backButtonText.enabled = true;
		}
		
		
		//#########################################player2 checks
		
		//check if the player can fire
		if (m_entity2.canAttack == true)
		{
			P2Attack.enabled = true;
		}
		else
		{
			P2Attack.enabled = false;
		}
		
		//check if the player can charge
		if (m_entity2.canCharge == true)
		{
			P2Charge.enabled = true;
		}
		else
		{
			P2Charge.enabled = false;
		}
		
		//check the player health
		// 100, 87.5, 75, 62.5, 50, 37.5, 25, 12.5, 0
		if (m_entity2.energyLevel <= 100 && m_entity2.energyLevel > 87.5) 
		{
			P2Health0.enabled = true;
			P2Health1.enabled = false;
			P2Health2.enabled = false;
			P2Health3.enabled = false;
			P2Health4.enabled = false;
			P2Health5.enabled = false;
			P2Health6.enabled = false;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = false;
			P2Sparks.startSize = 1;
		} 
		if (m_entity2.energyLevel < 87.5 && m_entity2.energyLevel > 75) 
		{
			P2Health0.enabled = false;
			P2Health1.enabled = true;
			P2Health2.enabled = false;
			P2Health3.enabled = false;
			P2Health4.enabled = false;
			P2Health5.enabled = false;
			P2Health6.enabled = false;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = false;
			P2Sparks.startSize = 1;
		} 
		if (m_entity2.energyLevel < 75 && m_entity2.energyLevel > 62.5) 
		{
			P2Health0.enabled = false;
			P2Health1.enabled = false;
			P2Health2.enabled = true;
			P2Health3.enabled = false;
			P2Health4.enabled = false;
			P2Health5.enabled = false;
			P2Health6.enabled = false;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = false;
			P2Sparks.startSize = 1;
		} 
		if (m_entity2.energyLevel < 62.5 && m_entity2.energyLevel > 50) 
		{
			P2Health0.enabled = false;
			P2Health1.enabled = false;
			P2Health2.enabled = false;
			P2Health3.enabled = true;
			P2Health4.enabled = false;
			P2Health5.enabled = false;
			P2Health6.enabled = false;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = false;
			P2Sparks.startSize = 1;
		}
		if (m_entity2.energyLevel < 50 && m_entity2.energyLevel > 37.5)
		{
			P2Health0.enabled = false;
			P2Health1.enabled = false;
			P2Health2.enabled = false;
			P2Health3.enabled = false;
			P2Health4.enabled = true;
			P2Health5.enabled = false;
			P2Health6.enabled = false;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = true;
			P2Sparks.startSize = 1;
		}
		if (m_entity2.energyLevel < 37.5 && m_entity2.energyLevel > 25) 
		{
			P2Health0.enabled = false;
			P2Health1.enabled = false;
			P2Health2.enabled = false;
			P2Health3.enabled = false;
			P2Health4.enabled = false;
			P2Health5.enabled = true;
			P2Health6.enabled = false;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = true;
			P2Sparks.startSize = 1;
		} 
		if (m_entity2.energyLevel < 25 && m_entity2.energyLevel > 12.5)
		{
			P2Health0.enabled = false;
			P2Health1.enabled = false;
			P2Health2.enabled = false;
			P2Health3.enabled = false;
			P2Health4.enabled = false;
			P2Health5.enabled = false;
			P2Health6.enabled = true;
			P2Health7.enabled = false;
			P2Sparks.enableEmission = true;
			P2Sparks.startSize = 1;
		} 
		if (m_entity2.energyLevel < 12.5 && m_entity2.energyLevel > 0) 
		{
			P2Sparks.startSize = 3;
		}
		if (m_entity2.energyLevel <= 0) //dead state
		{
			P2Health0.enabled = false;
			P2Health1.enabled = false;
			P2Health2.enabled = false;
			P2Health3.enabled = false;
			P2Health4.enabled = false;
			P2Health5.enabled = false;
			P2Health6.enabled = false;
			P2Health7.enabled = true;
			P2Sparks.enableEmission = false;

			player1WinText.enabled = true;
			backButton.enabled = true;
			backButtonText.enabled = true;
		}
	}
}