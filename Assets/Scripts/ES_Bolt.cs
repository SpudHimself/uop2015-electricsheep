﻿using UnityEngine;
using System.Collections;

#region Enums and Requirements
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
#endregion

public class ES_Bolt : MonoBehaviour 
{
	#region Fields
	//private fields
	private Transform m_transform;
	//public bad-boys
	public float movementSpeed;
	public float damage;
	public float lifeTime;

	public AudioClip fireSound;
	public AudioClip hitPlayer;
	public AudioClip hitWall;
	#endregion

	#region Unity Methods
	void Awake()
	{
		//caching transform
		m_transform = this.transform;
		Debug.Log ("fired");

		AudioSource.PlayClipAtPoint (fireSound, m_transform.position);
	}

	void Start() 
	{
		
	}

	void Update() 
	{
		if (lifeTime > 0.0f)
		{
			m_transform.Translate(Vector3.forward.normalized * movementSpeed * Time.deltaTime);
			lifeTime -= Time.deltaTime;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		for (int i = 0; i < 2; i++)
		{
			if (other.gameObject.tag == "Player" + ++i)
			{
				ES_PlayerEntity player = other.gameObject.GetComponent<ES_PlayerEntity>();
				player.energyLevel -= damage;

				AudioSource.PlayClipAtPoint(hitPlayer, m_transform.position);

				Destroy(this.gameObject);
			}
			else
			{
				AudioSource.PlayClipAtPoint(hitWall, m_transform.position);

				Destroy(this.gameObject);
			}
		}
	}
	#endregion
}