﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ES_MenuSoundHandler : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler {  
	public AudioSource m_audioSource;
	public AudioClip m_audioClipScroll;
	public AudioClip m_audioClipClick;
	
	public void OnPointerEnter( PointerEventData ped ) 
	{
		m_audioSource.PlayOneShot (m_audioClipScroll, 1.0f);
	}
	
	public void OnPointerDown( PointerEventData ped ) 
	{
		m_audioSource.PlayOneShot (m_audioClipClick, 1.0f);

	}    
}