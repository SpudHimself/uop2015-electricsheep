﻿using UnityEngine;
using System.Collections;

public class ES_menuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadScene(string levelName)
	{
		Application.LoadLevel(levelName);
	}

	public void QuitGame()
	{
		Application.Quit ();
	}
}
