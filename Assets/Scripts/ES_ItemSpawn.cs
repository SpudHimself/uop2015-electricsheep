﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ES_ItemSpawn : MonoBehaviour 
{
	public GameObject energyPickup;
	public List<GameObject> attackPickups = new List<GameObject>();
	public List<GameObject> spawnPoints = new List<GameObject>();

	public float itemDropTimer;
	private float m_resetTimer;

	private GameObject spawnedEnergy;
	// Use this for initialization
	void Start () 
	{
		m_resetTimer = itemDropTimer;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(itemDropTimer > 0.0f)
		{
			itemDropTimer -= Time.deltaTime;
		}
		else
		{
			Instantiate (attackPickups[Random.Range(0, attackPickups.Count)], spawnPoints[Random.Range (0, spawnPoints.Count)].transform.position, Quaternion.identity);
			itemDropTimer = m_resetTimer;
		}

		if (!spawnedEnergy)
		{
			spawnedEnergy = Instantiate(energyPickup, spawnPoints[Random.Range(0, spawnPoints.Count)].transform.position, Quaternion.identity) as GameObject;
		}
	}
}
