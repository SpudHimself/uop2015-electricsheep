﻿using UnityEngine;
using System.Collections;

public class ES_CameraZoom : MonoBehaviour
{
	#region Fields
	public GameObject player1;
	public GameObject player2;

	public float zoomThreshold;
	public float movementSpeed;
	public float rotationSpeed;

	public float cameraIncline;

	private Vector3 m_midPoint;
	private Camera m_camera;

	private ES_PlayerEntity entity1;
	private ES_PlayerEntity entity2;
	#endregion

	#region Unity Methods
	void Awake ()
	{
		m_camera = Camera.main;

		entity1 = player1.gameObject.GetComponent<ES_PlayerEntity>();
		entity2 = player2.gameObject.GetComponent<ES_PlayerEntity>();
	}

	void Start () 
	{
	}

	void Update () 
	{
		bool isEntity1Dead = entity1.IsDead();
		bool isEntity2Dead = entity2.IsDead();

		Debug.Log(isEntity1Dead);
		Debug.Log(isEntity2Dead);

		if(!isEntity1Dead && !isEntity2Dead)
		{
			//get the middle point between the two vectors, stored as a vector
			m_midPoint = (((player1.transform.position - player2.transform.position) * 0.5f) + player2.transform.position);
			Debug.DrawLine(transform.position, m_midPoint, Color.red);
			//rotates and track the mid point
			CameraFollowPoint (m_midPoint);

			float dist = Vector3.Distance (player1.transform.position, player2.transform.position);
			//makes sure that the camera doesn't zoom to a point where you can't see either player.
			if (dist < zoomThreshold) 
			{
				dist = zoomThreshold;
			}
			//calculates the new camera position
			Vector3 newPos = new Vector3 (m_midPoint.x, m_midPoint.y + cameraIncline, -dist - 2.0f);
			// smoothly interpolates from the current position to the new position
			m_camera.transform.position = Vector3.Lerp (m_camera.transform.position, newPos, movementSpeed * Time.deltaTime);
		}

		if (!isEntity1Dead && isEntity2Dead) 
		{
			Debug.DrawLine(m_camera.transform.position, player1.transform.position);
			CameraFollowPoint(player1.transform.position);
			
			Vector3 newPos = new Vector3 (player1.transform.position.x, player1.transform.position.y + 1.0f, -3.0f);
			m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, newPos, movementSpeed * Time.deltaTime);

			entity1.canBeControlled = false;
		}

		if (isEntity1Dead && !isEntity2Dead)
		{
			Debug.DrawLine(m_camera.transform.position, player2.transform.position);
			CameraFollowPoint(player2.transform.position);
			
			Vector3 newPos = new Vector3 (player2.transform.position.x, player2.transform.position.y + 1.0f, -3.0f);
			m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, newPos, movementSpeed * Time.deltaTime);

			entity2.canBeControlled = false;
		}
	}
	#endregion

	#region Methods
	public void CameraFollowPoint(Vector3 point)
	{
		Quaternion newRot = Quaternion.LookRotation (point - m_camera.transform.position);
		m_camera.transform.rotation = Quaternion.Slerp (m_camera.transform.rotation, newRot, rotationSpeed * Time.deltaTime);
	}

	public void ZoomInOnPlayer(GameObject player)
	{
		Debug.DrawLine(m_camera.transform.position, player.transform.position, Color.red);
		CameraFollowPoint(player.transform.position);
		
		Vector3 newPos = new Vector3 (player.transform.position.x, player.transform.position.y + 1.0f, -3.0f);
		m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, newPos, movementSpeed * Time.deltaTime);
	}
	#endregion
}
