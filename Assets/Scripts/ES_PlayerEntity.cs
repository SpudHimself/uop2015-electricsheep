﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ES_PlayerEntity : ES_BaseEntity 
{
	//if its public its for debugging
	#region Fields
	private Rigidbody m_rb;
	private Collider m_collide;
	private Animator m_animator;
	private AudioSource m_audioSource;
	private Renderer m_helmetRenderer;
	
	public float startingEnergyLevel;
	public float moveSpeed;
	public float rotSpeed;
	public float jumpingSpeed;
	public float energyReduceSpeed;

	public GameObject firingLocation;
	public GameObject bolt;
	public GameObject helmet;
		
	public bool canBeControlled;
	//input variables
	public float currentSpeed;
	private bool m_jump;
	private bool m_fire;
	private bool m_charge;

	private bool m_hasPlayedPowerDown;
	private bool m_hasPlayedChargeSound;

	public List<AudioClip> jumpSounds = new List<AudioClip>();
	public List<AudioClip> chargeImpactSounds = new List<AudioClip>();
	public List<AudioClip> landingSounds = new List<AudioClip>();
	public List<AudioClip> wallImpactSounds = new List<AudioClip>();
	public AudioClip chargeAttackSound;
	public AudioClip powerDownSound;
	public AudioClip attackPowerUpSound;

	private float m_chargingTime;
	private float m_resetTimer;
	private bool m_isCharging;
	private bool m_isFalling;

	private bool m_pressedCharge;
	#endregion

	#region Unity Methods
	public override void Awake()
	{
		base.Awake();

		m_rb = gameObject.GetComponent<Rigidbody>();
		m_collide = GetComponent<Collider>();
		m_animator = gameObject.GetComponent<Animator> ();
		m_audioSource = gameObject.GetComponent<AudioSource>();
		m_helmetRenderer = helmet.gameObject.GetComponent<Renderer> ();
	}
	// Use this for initialization
	public override void Start() 
	{
		base.Start();
		//stats
		energyLevel = startingEnergyLevel;
		//movement
		movementSpeed = moveSpeed;
		rotationSpeed = rotSpeed;
		jumpSpeed = jumpingSpeed;
		canAttack = false;
		canCharge = false;

		energyReductionSpeed = energyReduceSpeed;

		m_chargingTime = 1.0f;
		m_resetTimer = m_chargingTime;
		m_isCharging = false;

		m_helmetRenderer.enabled = false;
		m_hasPlayedPowerDown = false;
		m_hasPlayedChargeSound = false;

		canBeControlled = true;
	}

	public override void Update()
	{
		base.Update();	

		m_animator.SetFloat ("currentSpeed", currentSpeed);
		m_animator.SetBool ("jumping", m_jump);
		m_animator.SetBool ("isGrounded", isGroundedCheck ());
		m_animator.SetBool ("isFalling", m_isFalling);
		m_animator.SetBool ("isCharging", m_isCharging);
		m_animator.SetBool ("isDead", IsDead ());

		if (canCharge)
		{
			m_helmetRenderer.enabled = true;
		}
		else
		{
			m_helmetRenderer.enabled = false;
		}
	}
	// Update is called once per frame
	public override void FixedUpdate() 
	{
		base.FixedUpdate();
		//movement
		if(!IsDead() && canBeControlled)
		{
			//checking the player
			if (gameObject.tag == "Player1")
			{
				currentSpeed = Input.GetAxis ("P1_Horizontal");
				m_jump = Input.GetButton ("P1_Jump");
				m_fire = Input.GetButton ("P1_Fire");
				m_charge = Input.GetButton("P1_Charge");
			}
			if (gameObject.tag == "Player2")
			{
				currentSpeed = Input.GetAxis ("P2_Horizontal");
				m_jump = Input.GetButton ("P2_Jump");
				m_fire = Input.GetButton ("P2_Fire");
				m_charge = Input.GetButton("P2_Charge");
			}

			Debug.Log(m_charge);

			if (canAttack)
			{
				if (!m_audioSource.isPlaying)
				{
					m_audioSource.clip = attackPowerUpSound;
					m_audioSource.Play();
				}
			}

			if(m_fire)
			{
				Attack();
			}

			if(m_charge)
			{
				//Charging();
				if(canCharge)
				{
					m_pressedCharge = true;
					m_isCharging = true;
				}
			}

			if(m_pressedCharge)
			{
				Charging();
			}

			Vector3 movement = new Vector3 (currentSpeed, 0.0f);
			m_rb.AddForce ((movement * movementSpeed), ForceMode.Acceleration);
			transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f);

			if (currentSpeed > 0) 
			{
				Quaternion newRot = Quaternion.LookRotation (Vector3.right);
				transform.rotation = Quaternion.Slerp (transform.rotation, newRot, rotationSpeed * Time.deltaTime);
			}
			
			if (currentSpeed < 0) 
			{
				Quaternion newRot = Quaternion.LookRotation (Vector3.left);
				transform.rotation = Quaternion.Slerp (transform.rotation, newRot, rotationSpeed * Time.deltaTime);
			}

			//jumping 
			if (m_jump && isGroundedCheck()) 
			{
				m_rb.velocity = new Vector3(m_rb.velocity.x, jumpSpeed, m_rb.velocity.z);
				m_audioSource.PlayOneShot(jumpSounds[Random.Range(0, jumpSounds.Count)]);
			}
		}

		else
		{
			if(!m_hasPlayedPowerDown)
			{
				m_audioSource.PlayOneShot(powerDownSound);
				m_hasPlayedPowerDown = true;
			}

			currentSpeed = 0.0f;
			energyReductionSpeed = 0;
		}

		if (m_rb.velocity.y < -1.0f)
		{
			m_isFalling = true;
		}
		else
		{
			m_isFalling = false;
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if(m_isCharging)
		{
			if (col.gameObject.tag == "Player1" || col.gameObject.tag == "Player2")
			{
				col.gameObject.GetComponent<ES_PlayerEntity>().energyLevel -= 30.0f;
				m_audioSource.PlayOneShot(chargeImpactSounds[Random.Range(0, chargeImpactSounds.Count)]);
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Floor")
		{
			m_audioSource.PlayOneShot(landingSounds[Random.Range(0, landingSounds.Count)]);
		}

		if(other.gameObject.tag == "Wall")
		{
			m_audioSource.PlayOneShot(wallImpactSounds[Random.Range(0, wallImpactSounds.Count)]);
		}
	}
	#endregion

	#region Methods
	private bool isGroundedCheck()
	{
		return Physics.Raycast (transform.position, Vector3.down, (m_collide.bounds.extents.y));
	}

	public void Attack()
	{
		if (canAttack)
		{
			Instantiate(bolt, firingLocation.transform.position, firingLocation.transform.rotation);
			canAttack = false;
		}
		/*
		if (canCharge)
		{
			Debug.Log("Charged");
			m_isCharging = true;
		}
		*/
	}

	public void Charging()
	{
		if(m_isCharging)
		{
			if (!m_hasPlayedChargeSound)
			{
				m_audioSource.PlayOneShot (chargeAttackSound);
				m_hasPlayedChargeSound = true;
			}

			gameObject.layer = 0;
			if (m_chargingTime > 0.0f)
			{
				m_chargingTime -= Time.deltaTime;
				Vector3 movement = new Vector3 (currentSpeed, 0.0f);

				if (currentSpeed > 0)
				{
					m_rb.AddForce ((movement * movementSpeed), ForceMode.Acceleration);
					transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f);
				}
				else
				{
					m_rb.AddForce ((movement * movementSpeed), ForceMode.Acceleration);
					transform.position = new Vector3(transform.position.x, transform.position.y, 0.0f);
				}
			}
			else
			{
				if(gameObject.tag == "Player1")
				{
					gameObject.layer = 8;
				}

				if(gameObject.tag == "Player2")
				{
					gameObject.layer = 9;
				}

				m_chargingTime = m_resetTimer;
				m_isCharging = false;
				canCharge = false;
				m_pressedCharge = false;

				m_hasPlayedChargeSound = false;
			}
		}
	}
	#endregion
}
