﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region Enums and Components
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
#endregion

public class ES_EnergyItem : ES_BaseItem 
{
	#region Fields
	private Transform m_transform;
	//public setters in the inspector
	public float rotSpeed;
	public float hovHeight;
	public float downSpeed;
	public float energyBoostAmount;

	public List<AudioClip> pickupSounds = new List<AudioClip>();
	#endregion

	#region Unity Methods
	public override void Awake()
	{
		//call parent Awake method
		base.Awake();
		//private default initialisation
		m_transform = this.transform;
	}

	public override void Start()
	{
		//call parent Start Method
		base.Start();
		//default initialisation for values in inspector
		rotationSpeed = rotSpeed;
		hoverHeight = hovHeight;
		fallSpeed = downSpeed;
	}
	// Update is called once per frame
	public override void Update() 
	{
		//call parent Update method
		base.Update();	
	
		if (!AboutToCollide())
		{
			m_transform.Translate(Vector3.down * fallSpeed * Time.deltaTime);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		for (int i = 0; i < 2; i++)
		{
			if (other.gameObject.tag == "Player1" || other.gameObject.tag == "Player2") 
			{
				Debug.Log("Touched a player");

				ES_PlayerEntity player = other.gameObject.GetComponent<ES_PlayerEntity>();
				player.energyLevel += energyBoostAmount;

				if (player.energyLevel > 100.0f)
				{
					player.energyLevel = 100.0f;
				}

				AudioSource.PlayClipAtPoint(pickupSounds[Random.Range(0, pickupSounds.Count)], m_transform.position);

				Destroy (this.gameObject);
			}
			else
			{
				Debug.Log("did not touch a player.");
				Destroy(this.gameObject);
			}
		}
	}
	#endregion

	#region Methods
	#endregion
}
