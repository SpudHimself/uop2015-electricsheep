﻿using UnityEngine;
using System.Collections;

#region Enums and Components
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
#endregion

public class ES_AttackItem : ES_BaseItem 
{
	#region Fields
	private Transform m_transform;
	//public setters in the inspector
	public float rotSpeed;
	public float hovHeight;
	public float downSpeed;

	public AudioClip pickupSound;
	#endregion

	#region Unity Methods
	public override void Awake()
	{
		//call parent Awake Method
		base.Awake ();
		//default initialisation
		m_transform = this.transform;
	}

	public override void Start() 
	{
		//call parent Start Method
		base.Start ();
		//set default values from inspector
		rotationSpeed = rotSpeed;
		hoverHeight = hovHeight;
		fallSpeed = downSpeed;
	}

	public override void Update() 
	{
		//call parent Update Method
		base.Update ();
		
		if (!AboutToCollide())
		{
			m_transform.Translate(Vector3.down * fallSpeed * Time.deltaTime);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (gameObject.tag == "AttackItem")
		{
			if (other.gameObject.tag == "Player1" || other.gameObject.tag == "Player2") 
			{
				Debug.Log("Touched a player");

				ES_PlayerEntity player = other.gameObject.GetComponent<ES_PlayerEntity>();
				player.canAttack = true;

				AudioSource.PlayClipAtPoint(pickupSound, m_transform.position);

				Destroy (this.gameObject);
			}
			else
			{
				Debug.Log("did not touch a player.");
				Destroy(this.gameObject);
			}
		}

		if (gameObject.tag == "ChargeItem")
		{
			if (other.gameObject.tag == "Player1" || other.gameObject.tag == "Player2") 
			{
				Debug.Log("Touched a player");
				
				ES_PlayerEntity player = other.gameObject.GetComponent<ES_PlayerEntity>();
				player.canCharge = true;
				
				AudioSource.PlayClipAtPoint(pickupSound, m_transform.position);
				
				Destroy (this.gameObject);
			}
			else
			{
				Debug.Log("did not touch a player.");
				Destroy(this.gameObject);
			}
		}
	}
	#endregion
}
