﻿using UnityEngine;
using System.Collections;

public class ES_GlobalMusic : MonoBehaviour 
{
	private static ES_GlobalMusic m_instance = null;

	public static ES_GlobalMusic GetInstance
	{
		get { return m_instance; }
	}

	void Awake()
	{
		if(m_instance != null && m_instance != this)
		{
			Destroy(this.gameObject);
			return;
		}

		else
		{
			m_instance = this;
		}

		DontDestroyOnLoad(this.gameObject);
	}
}
